(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.billingAddressSync = {
    attach: function (context, settings) {
      $(document).ready(function () {
        var form = $('.commerce-checkout-flow');
        form.find(':input[name*="[billing_information][address][0][address]"]').each(function () {
          // Extract the address field name.
          var name = $(this).attr('name').split('[');
          name = name[name.length - 1].replace(']', '');

          var input = $(':input[name*="[billing_information][address][0][address][' + name + ']"]');
          input.on('keyup', function () {
            drupalSettings.billingAddress.value[name] = input.val();
          });
        });
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
